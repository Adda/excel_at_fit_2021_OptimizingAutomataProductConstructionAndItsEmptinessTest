## Fixed mistakes, typos and others from the reviews
### Typos
- (148) "Language" => "language"
- (252) "Length" => "length"

### Improvements of text format
- Kdekoli to není špatně, tak volte v technickém textu přítomný čas místo minulého či dokonce "could/would" (vyjadřují pravděpodobnost, tj. ne fakta). Např. (28), ...
- Zkráceniny jako it's, isn't, don't, let's a další do technického textu nepatří (např. (70), (241), (253), ...)
- 189: yield
- footnote 2: move this information into section 4
- abstract: Finite automata theory is a ...
- abstract: which is _time_ ?
- abstract: unnecessary state space
- (abstrakt) "field" => "model"; "theory, and we" => "theory. We", "time and again" vypustit, "so-called" a "in the end" klidně vypustit, člen před "length abstraction"
- 19: "marginally useful" sounds like it is not very useful
- 21: runs where (no comma)
- 25: such a run
- (28) "we would start" => "the classical algoritm" nebo "the classical method" (to would vyjadřuje nejistotu)
- 32: find their
- 55: accept state -> accepting/final state (in many other places in the paper, please fix all)
- (55) a další: "accept state" => "accepting state" (zkontrolujte v literatuře, ale mě to accept state nepřijde správné)
- 70: don't -> do not (in many other places)
- 78: as in [11]
- (91) "We are counting" ... asi špatné sloveso, nedává mi to smysl, možná "computing" nebo "evaluating"?
- 91: counting -> computing
    - Computing.
- (105-106) možná nadbytečné, zvažte vypuštění
- (127-129) Možno zkrátit, vše vyjadřuje nadpis.
- (159 a 165) zde je to nějaké divné, pokud P(Q) je "power set of Q", tak potom platí $Q \in P(Q)$. $P(Q)$ nemůže být jen množina, protože potom by to nemohlo být takto v signatuře $\delta$ na řádku 158.
- 159: P(Q) is not a set of states from Q
    - `P(Q)` should be a set of subsets of `Q`. Fixed.
- 165: the sentence Determinization is ... is strange
    - I have rewritten the while paragraph to beter resemble what I was trying to describe.
- (176-177) Toto je již řečeno na (170-172), takže se zbytečně neopakujte.
- 177: said to be equivalent -> have the same languages?
    - Already deleted.
- (183) $I' = \{I\}$ je špatně, protože $I$ je množina stavů dle def. 2.2; dále by pro $\delta'$ mělo být řečeno, co je $S$ (uvnitř $F$ je to v jiném kontextu a může jít o jiný symbol)
    - Both notes are valid. Rewritten.
- (184-187) vypustit - pro článek irelevantní podle mě
- (188) "Interactions"? (nechtěl jste říci intersections?) celé je to divně naformulované.
    - The intent was to say the product is a result of operations on finite automata. Using operations from now on.
- 188: overflow
- 195: what is the results of A1 x A2 ? These are not sets.
    - That is right, I was wrong. The line was deleted as misleading, furthermore, the product was already define above. The intet was to depict the product as a result of operations on finite automata, but poorly written.
- 197: why is there Q^2?
    - My mistake, `Q` is a set of states in product automaton. Thereofore, only `Q` shoud be in product relation.
- (200) (kromě přetečení řádku, což je i na dalších místech) je zde nová nedefinovaný notace pro "product state" v hranatých závorkách.
    - 'product state' defined.
- 200: overflow
- (202) Když už máte $A_1$ a $A_2$ definované, tak není třeba pořád říkat, co to je, stačí napsat "from $A_1$ and $A_2$, respectively, ..."
- 206: overflow
- 207, eq. below: I guess I know what the author is trying to say, but this is not the right way to do it.
- 210: a finite
- (217) na druhou stranu, zde není definováno co je to "emptiness", klasicky to bývá problém zjistění, že $L(A) = \emptyset$, ale asi by to chtělo aspoň jednou specifikovat (stačí stručně formálně bez okecávání)
    - The new notion in the Preliminaries chapter depicting emptiness test was added.
- (208-209) vypustit, jen mě zarazil nový pojem "synchronous", který pak ale dál není využit, takže jej nezavádějte.
- (210-213) an finite => a finite; ale jinak klidně tuto část vypustit
    - The whole paragpraph was omitted.
- (221) "while" ... trochu mi to sem nesedí, co spíše "and then"? (ve smyslu "check lengths and then check the words")
- (224-230) při prvním čtení mi nebylo jasné, co chcete říci, tak zvažte vyladění
- 253: Let's -> Let us
- (256) article => paper (article je spíše novinový článek než vědecký článek)
- (258) "Transitions" ... \delta je funkce a ta má dvojice, protože matematicky to je relace. Transitions zní jako vliv IFJ, ale použitá definice je jiná než v IFJ!
- 259: fact that we work
- (260) neživotné podstatné jméno mývá přidavné jméno často bez "'s" tj. např. word lengths nebo automaton grammar, kde ale asi chcete spíše napsat "automaton alphabet", ne?
- 260: grammar -> alphabet?
- Chybějící čárka (např. za "Then" na (261), za "Here" na (385)).
- (270-272) Je to příliš upovídané, stačilo by "For the final HL automaton A'_1, see Figure 3."
- 271: from Figure
- (278) "the orginal automaton" => $A_1 ?
- 284: ineffective -> inefficient
- 315: shows how
- (321) "otherwise processing further" - to zní neobratně
- 321: and other processing
- (323-330) tato část je celkem jasná a mohla by být vypuštěna nebo značně zkrácena
    - Deleted.
- 325: is the set of
- 323: tells -> denotes
- 328: The optimization process starts when
- Poznámka pod čarou 4 - takže se použije Depth-first Search nebo je to ještě jiný způsob prohledávání grafu?
    - Indeed, it is Depth-first Search algorithm for a graph in fact.
- footnote 4: fact that more ... a non-empty ... This works even better
- (340) "both of the" => "both"
- (341) "section" => "Section" (stejně jako "Chapter" a "Figure") - odkazovatelné části textu se píší velkým písmenem když mají u sebe i číselnou identifikaci
- (342) "formulae" => "formula"
- 342: formulae in the form
    - Formulae is intentional, it is possible to get multiple different formulae for a single state (up to one for every final state)
- Fig. 4 - NDA $A_2$? (očividně to není DFA)
- Figure 4: the automaton is not deterministic
- 345: k represents... this sentence is confusing
    - The sentence was rewritten to better depict the role of 'k' in the expression.
- 352: an SMT solver
- footnote 5: in section 3.1 ... in section 3.2
- 356: \delta_2
- (357) In Figure 5, there is ...
- 360+3: Figure~3
- 360+5: Figure~5
- (361) ", that" => "such that"
- 361: find values of k and l such that
- 364: an SMT solver
- 370: length-
(375) "states and their" => "state and its"
- (377) "In the Figure 6 we" => "In Figure 6, we"
- (379) "states" => "product states"
- (381) "-" => "---" (bez mezer kolem!)
    - U spojovníku si zjistěte, zda spíše nechcete použít --- (em dash) používaný pro oddělení vložených vět.
- (382) "would be normally" => "are"
- 377: In Figure
- 377: see the product -- which product? of what?
- (378 a 383) zrušte barvy u Red a Green, jednak je ta zelená špatně kontrastní a působí to spíše pouťově; kdo není barvoslepý, ví jak vypadá zelená a červená
- (384) "states were" => "state is"?  (kdy je ten stav jen jeden)
- 386: both original automata
- (391) "the Figure" => "Figure"
- 389: resolving: find a better word
- 391: in Figure
- Alg.2:, l10: if sat then
- 402: branch there cannot
- 408: say that such .. have an empty
- 414: decidable - find a better word
- 428: word's -> words' (should be plural, right?)
- (429) tím "the emptiness test" máte na mysli řádek 9 v Alg. 2?
- 434: get true
- (438) "state" => "product state"
- (456)  tyto nevětvené větve se v teorii grafů nazývají linky (Lines)
- (461) "For better imagination, in the Algorithm" => "In Algorithm"
- 461: in Algorithm
- 478: The blue state in Figure~8
- 493: or skip checking satisfiability in the (now it seems that in both cases, we don't need to process the branch)
- 497: from regular model checking. (remove the rest of the sentence)
- 521: along the way
- 523: the full product
- 526: The graph in Figure~10
- 531: The graph in Figure~9
- 541: are satisfiable -> have the skippable predicate satisfiable?
    - No, most of the processed states are evaluated as *satisfiable* in length abstraction satisfiable check.
- 544: Typical... - I don't understand what the sentence is saying
    - In full product construction results, if there are nearly no product states to trim, the generated product state space size \emph{explodes} similarly to the basic product construction algorithm---typical for automata with large amounts of transitions from every state causing large amounts of possible accepted lengths, where our algorithm can trim only a few states.
- 548: In other... - I don't understand what the sentence is saying -> make it clearer
- (515) "state space" je pouze jeden, počítat má smysl například "product states" nebo "size of the state space"
- (519-524) tato část tedy není ve Fig. 10 ale pouze v materiálech na Google sheet? Poznamenejte to případně.
- (526 a 531) "The graph" => "Figure"
- (575) "graph" => "Figure";
- 561: has an empty
- 575: In the graph in Figure~10
- 580,584: tests -> experiments
- (580-592) tato část má velký překryv se začátkem Conclusion - navrhuji spoji a ušetřit tak několik dalších řádků
- 590: usages -> uses
- 592: reducting the size of the generated state space.
- (598 a 599) -- => --- (a bez mezer z obou stran!)
- 616: using Parikh's theorem
- 619: Thus - find a better connecting word

### References
- Pozor na chování BibTeX, který automaticky normalizuje velikosti písmen a nepozná zkratky apod. - je třeba takový kus textu obalit do extra složených závorek (např. [15]: büchi => Büchi, [2]: "a. trakhtenbrot" => "A. Trakhtenbrot" a případně další).
- (8-10) Opravdu je potřeba tolik citací? Pro ilustraci by stačila 1-3.
    - I have reduced the amount of references, I agree the former amount was a bit excessive.
- 8: the references seem to be for decision procedures of logics rather than model checking
    - The references mention model checking but I decided to use other references that depicts the problem better.
- 9: references 4,5,6,8 are for separation logic or heap abstractions, not string solving
    - The references mention string solving and analysis, but I decided to use other references that depicts the problem better.
- [2]: Boris A. Trakhtenbrot, [15]: buchi -> Buchi
- ad [13] online => Online a chybí zde URL!
- [13]: give a link
- ad [14] "3 edition" => "3rd Edition"
- ad [15] Pokud nemáte měsíc u všech citací, tak uvádějte jen rok (tj. vypustit "12 ")
- ad [16] "2009---lp4 2009" ... vypustit?, opět "online" a opět chybí "URL"
- Algorithm 1: is it referenced from somewhere?
    - It is, as well as majority of the preliminaries. I added a reference for definition in preliminaries.
- Někde je u rozsahu stránek "page" a někde "pages" sjednotit na "pages" nebo na "p."Jednotlivé typy publikací by měly mít jednotné/konzistentní formátování i uváděné množství informací (včetně toho, co dáváte kurzívou apod.) - např. v [4] je kurzívou i "Volume ...", ale v [5] ne; množství detailů
[9] a [11] je nepoměrně větší než u dalších konferenčních příspěvků jako [4] a [5]; v [3] máte kurzívou název místo časopisu; v [7] a [8] to vypadá, že vám chybí Volume a Number toho časopisu).
    - Fixed references format and updated some references.
- references: [13-16]: not referenced in the paper
    - Some references were removed and others explicitly cited in the text.

## Changed content
- 30: transitions which (no comma --- also in many other places, please check when to use comma in relative clauses, i.e., what is the difference between defining and non-defining relative clause)
    - I tried to fix all relative clauses and add commas where required.
- délka příspěvku přesahuje 10 stran, takže navrhuji zkrátit alespoň na 10 stran (níže jsou nějaké tipy)
    - I tried to minimize the amount of text as much as possible.
- Figure 6: I suggest to use disjoint state sets for the two NFAs, to avoid confusion (moreover, please use $q_1$ instead of $q1$, at least in the text)
    - I changed the state sets to two disjoint sets and make all numbers in state names subscript.

- Obr. 3: Pokud A'_1 vzniklo determinizací A_1 jen s *, tak mi to nesedí, protože by měl přijímat i řetězec délky 5, což automat na obrázku 3 nepřijímá! A neměl by být nazván ten automat jako "initial handle and loop automaton"? (nebo co v tom označení znamená slovo initial?)
    - I have forgotten to update the paper figures for the updated automata I have used as an example. The figures were updated and the corresponding text was slightly changed to better describe the figures.
- Alg. 3, line 9 - zavírací závorka by neměla být kurzívou (obecně žádná závorka); celkově je Alg. 3 poměrně nezajímavý a jasný již z předchozího popisu.
- Algorithm 3: is the only difference from Alg 2 is the "not skippable" test, I suggest to just say that line 9 in Alg 2 is subsituted with ...
    - I was considering this option earlier whe writing the paper. In the end, I chose to show the whole algorithm, but, as both reviews noted, showing only the changed lines is probably simpler for the reader to understand.
- I decided to use term `unary alphabet` for HLA alphabet as it describes the main point of HLA better than my own description.
- In section 4, I'd also expect to see how is it algorithm implemented
(implementation language, link to the repository) and overall run time
comparison.
    - The links for reference implementation were already in the paper but I added a paragraph at the start of section 4 to explicitly reference the implmentation and used libraries and SMT solver.
- Figure 9 & 10: the presentation of the graphs is not good. Find a better way to present the results! How about using a scatter plot instead of the graph in Fig. 10?  That should give a much clearer picture.
    - Using scatter graph was a great idea. I changed both used graphs to scatter graphs for both emptiness test and full product construction.
- Fig. 9: špatný kontrast při černobílém tisku (ty sloupce všechny až do 100% moc informací nenesou)
    - Percentage graph comparison was replaced by scatter graph.
- Fig. 10: zde jsem úplně nepohocpil co nese za informaci ta "Trend line", proč například ta základní nezačíná na začátku osy x?
- "trend lines" - není mi zcela jasné, co přesně mají reprezentovat (vypadá to trochu jako zobrazení trendu v lineárních osách, i když má graf logaritmické osy; popište lépe, co "trend lines" znamenají)
    - Due to using scatter graphs instead of column and percentage comparison graphs, the trend lines were removed as not valid representation of presented data.
- Emphasize when the axes (and which) are logarithmic!
    - Note about logarithmic scale added to the figure caption.
- (117-124) přijde mi, že jste vše jasně popsal v předchozím, takže toto můžete vypustit
    - Kept, but reducted its size.
- (130-134) stačilo by se odkázat na jakoukoli klasickou knihu o formálních jazycích či teorii automatů
    - I think those few lines are importat enought to keep them in the paper.





- bigger comment: the paper confuses an expression with a formula, in particular, instead of having an experssion "a+b*k", the author probably wants a formula of the form "\exists k(len = a + b*k)"; I suggest to really use formulae to avoid confusion later
    - That is true, I have changed the neccessary parts of the text according to this recommendation.

- Alg. 2, řádek 18: "a" => "$a$" (nad šipkou) (na více místech např. (205)); dále pozor, že tuto notaci máte definovanou pro výpočetní krok, ale ne jako prvek do funkce \delta (v IFJ se na to používal symbol $\vdash$, u gramatik by to bylo $\Rightarrow$)
    - I agree. I changed the notation to in the algorithm to previously defined notation instead (I don't want to unnecessarily define yet another notation for reader to grasp first).

- Možná by bylo jednoznačnější uvést formální algoritmus HLA místo textového popisu, kde je prostor pro nejasnosti.
    - That is true, but nevertheless, I would need to add an intuitive description for the reader to understand the reason for using lasso automata in our case without laboured reading of the formal algorithm. For an expanded version of the paper, for sure this is something to remember.

- 76: how about changing "handle and loop" automata to "lasso" automata?
    - I will do that. It is simpler and used quite often too.

- Sekce 3.2: Označte si pomocí symbolů jednotlivé "handle and loop automaton" (nebo aspoň zaveďte zkratku jako třeba "(HL automaton for short)") a "initial handle and loop automaton" a používejte v textu jen symboly. Přes mnoho "handle and loop automaton" se čtenář ve vlastním popisu brzy ztratí. Možná by bylo jednoznačnější uvést formální algoritmus místo textového popisu, kde je prostor pro nejasnosti.
I v ostatních sekcích, když nějaký odkaz na něco jako "original automaton" zopakujete více jak jednou, tak se vyplatí jej označit symbolem a raději jej použít místo textu (zvyší se jednoznačnost toho, o kterém automatu právě mluvíte). Jen je počet těch symbolů v rámci jedné sekce/kapitoly nutno držet na rozumném počtu (cca do 7 kusů). Není též jasné, co je to "expanded HL automaton" a na který automat se tímto vlastně odkazujeme?

- chybí alespoň stručná zmínka o podobných pokusech o optimalizaci klasického konstrukčního algoritmu a ideálně i nějaké základní srovnání
    - Nothing to compare that we know of, but might be worth investing into combining our approach with other already known approaches to product construction optimization.
- Secondly, it seems (it is not explicitly written, which is quite fishy by itself) that in the experimental evaluation, computation of the generated state space *does not* take into account the size of the constructed handle and loop automata.  This should definitively be mentioned.
    - A paragraph explaining why this is a case was added at the end of Experiments and Results section.
# to send

As avised, I applied most of the suggestions and prompts. I decided to use lasso automata instead of handle and loop automaton. Instead of unclear expression, I am using proper formulae instead. For the text to be clearer, I started using symbols to uniquely reference certain terms in text repeatedly used throughtout a (sub)chapter.
